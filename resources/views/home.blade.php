@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <form action="{{ route('search') }}" method="GET">

        <div class="input-group mb-3">
          <input type="search" name="q" class="form-control" placeholder="Search..." aria-label="Sea" aria-describedby="button-addon2">
          <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Search</button>
        </div>

      </form>

      @if(isset($posts) && $posts->count())
      <h6>Hasil pencarian anda adalah <strong>"{{ $query }}"</strong></h6>
      @foreach ($posts as $post)
          <ul>
              <li>{{ $post->title }}</li>
          </ul>
      @endforeach
      @else
      @endif
    </div>
  </div>
</div>
@endsection
